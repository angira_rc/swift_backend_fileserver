import os
import errno
import shutil
import json
from status import Status
import datetime

def file_hierarchy(path):
    os.system("cd users && git pull")
    hierarchy = {
        'type': 'dir',
        'name': os.path.basename(path),
        'path': path,
    }

    try:
        hierarchy['children'] = [
            file_hierarchy(os.path.join(path, contents))
            for contents in os.listdir(path)
        ]
    except OSError as e:
        if e.errno != errno.ENOTDIR:
            raise
        hierarchy['type'] = 'file'
        hierarchy['size'] = os.path.getsize(hierarchy['path'])

    return json.dumps(hierarchy)

def create_user_dir(username):
    path = 'users/'
    creation = os.mkdir(f"{path}{username}")
    os.system("cd users && git add -A")

    return creation

def read(data):
    os.system("cd users && git pull")
    try:
        file = open(f"{data['path']}{data['name']}", "r")
        data = {
            'contents': file.read()
        }

        return Status(True, data)

    except OSError as e:
        return Status(False)

def create(data, username):
    os.system("cd users && git pull")
    if data["type"] == "dir":
        creation = os.mkdir(f"{data['path']}{data['name']}")
        data = {
            'content': 'dir',
            'hierarchy': file_hierarchy(f"users/{username}")
        }
        return Status(True, data)

    elif data["type"] == "file":
        try:
            name = data['name']
            file = open(f"{data['path']}{name}", "x")
            file = open(f"{data['path']}{name}", "r")
            data = {
                'content': file.read(),
                'hierarchy': file_hierarchy(f"users/{username}")
            }

            saahii = datetime.datetime.now()
            os.system("cd users && git add -A")
            os.system(f'cd users && git commit -m "Created file `{name}` Date: {str(saahii.time())} Time: {str(saahii.time())}"')
            os.system("cd users && git push -u origin master")
            return Status(True, data)
        except OSError as e:
            data = {
                "error": "File already exists!"
            }
            return Status(False, data)

def rename(data, username):
    os.system("cd users && git pull")
    try:
        name = data['name']
        new_name = data['new']
        os.rename(f"{data['path']}{name}", f"{data['path']}{new_name}")
        data = {
                    'contents': data['new'],
                    'hierarchy': file_hierarchy(f"users/{username}")
                }

        saahii = datetime.datetime.now()

        os.system("cd users && git add -A")
        os.system(f'cd users && git commit -m "Renamed file `{name}` to `{new_name}` Date: {str(saahii.time())} Time: {str(saahii.time())}"')
        os.system("cd users && git push -u origin master")

        return Status(True, data)

    except OSError as e:
        return Status(False)

def modify(data, username):
    os.system("cd users && git pull")
    try:
        name = data['name']
        file = open(f"{data['path']}{name}", "w")
        file.write(data['contents'])
        file.close()
        file = open(f"{data['path']}{name}", "r")
        data = {
            'content': file.read(),
            'hierarchy': file_hierarchy(f"users/{username}")
        }
        saahii = datetime.datetime.now()

        os.system("cd users && git add -A")
        os.system(f'cd users && git commit -m "Modified file `{name}` Date: {str(saahii.time())} Time: {str(saahii.time())}"')
        os.system("cd users && git push -u origin master")

        return Status(True, data)

    except OSError as e:
        return Status(False)

def delete(data, username):
    os.system("cd users && git pull")
    name = data['name']
    if data['type'] == 'file':
        os.unlink(f"{data['path']}{data['name']}")
        data = {
            'content': 'file',
            'hierarchy': file_hierarchy(f"users/{username}")
        }
        saahii = datetime.datetime.now()
        
        os.system("cd users && git add -A")
        os.system(f'cd users && git commit -m "Deleted file `{name}` Date: {str(saahii.time())} Time: {str(saahii.time())}"')
        os.system("cd users && git push -u origin master")

        return Status(True, data)

    elif data['type'] == 'dir':
        shutil.rmtree(f"{data['path']}{data['name']}")
        data = {
            'content': 'file',
            'hierarchy': file_hierarchy(f"users/{username}")
        }
        
        os.system("cd users && git add -A")
        os.system(f'cd users && git commit -m "Deleted directory `{name}` Date: {str(saahii.time())} Time: {str(saahii.time())}"')
        os.system("cd users && git push -u origin master")

        return Status(True, data)

    
    return Status(False)
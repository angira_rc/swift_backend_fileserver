from flask import Flask, request, session, jsonify
from flask_cors import CORS, cross_origin
import directory as dr
from datetime import datetime
from status import Status

app = Flask(__name__)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'

peers = ['kaya']

@app.route('/dist', methods = ['GET'])
def active ():
    # Set git remote here
    return jsonify({ 'OK': True })

@app.route('/dist/user', methods = ['POST'])
def createUser ():
    req = request.get_json()
    dr.create_user_dir(req['username'])

    return jsonify(True)

@app.route('/dist/files/hierarchy', methods = ['POST'])
def hierarchy():
    req = request.get_json()
    hierarchy = Status(True, dr.file_hierarchy(f"users/{req['username']}"))

    return jsonify(hierarchy.json())

@app.route('/dist/files/read', methods = ['POST'])
def read ():
    data = request.get_json()
    files = dr.read(data)  
    return jsonify(files.json())

@app.route('/dist/files', methods = ['POST', 'PUT', 'DELETE'])
def files():
    data = request.get_json()
    username = data['username']
    print(username)
    files = None
    if request.method == 'POST':
        files = dr.create(data, username)

    elif request.method == 'PUT':
        if data['operation'] == "rename":
            files = dr.rename(data, username)

        elif data['operation'] == "modify":
            files = dr.modify(data, username)
            
    elif request.method == 'DELETE':
        files = dr.delete(data, username)
        
    return jsonify(files.json())


if __name__ == '__main__':
    app.secret_key = 'file_server_key'
    app.run(debug=True, port=5001)
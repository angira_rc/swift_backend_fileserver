class Status:
    default = {
        'error': "A problem was encountered."
    }
    def __init__(self, OK, data=default):
        self.OK = OK
        self.data = data

    def json(self):
        return {
            "OK": self.OK,
            "data": self.data
        }